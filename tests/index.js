const assert = require('assert')
const validateSchema = require('../src')
const { loadContent } = require('../lib/utils')

function testValidateSchema() {

  const person = {
    names: {
      first_name: 'Tom',
      last_name: 'Xoman'
    },
    id: 45,
    nons: "nons",
    height: 123,
    hobbies: [ "gödel", "escher", "bach" ],
    attributes: [
      { foo: "bar" },
      { foo: "baz" }
    ]}

  const wrongPerson = {
    person: {
      names: {
        last_name: 'Namox'
      },
      id: 45,
      nons: ["nons"],
      height: "123",
      hobbies: [ "gödel", "escher", "bach" ],
      attributes: [
        { foo: "bar" },
        { foo: "baz" }
      ]}
    }

  const requiredSchema = {
    names: {
      first_name: { type: "string", required: true },
      last_name: { type: "string" }
    },
    id: { type: "number" },
    nons: { type: "string" },
    height: { type: "number" },
    hobbies: [{type: "string" }],
    attributes: [
      { foo: { type: "string" } }
    ]
  }

  // actual tests
  function shouldPassForCorrectObject() {
    //let person = { name: { first: 'Tom', last: 'Xoman' }, age: 45 }
    schemaErrors = validateSchema(person, { schema: requiredSchema, logLevel: 'none' })
    if(!Array.isArray(schemaErrors)) {
      assert.fail('Returned object must be an array')
    }
    assert.strictEqual(schemaErrors.length, 0)
  }

  function shouldPassForCorrectObjectFromFile() {
    schemaErrors = validateSchema(person, { schemaPath: `${__dirname}/../examples/schema.json`, logLevel: 'none' })
    if(!Array.isArray(schemaErrors)) {
      assert.fail('Returned object must be an array')
    }
    assert.strictEqual(schemaErrors.length, 0)
  }

  function shouldPassForCorrectObjectFromYamlFile() {
    schemaErrors = validateSchema(person, { schemaPath: `${__dirname}/../examples/schema.yaml`, logLevel: 'none' })
    if(!Array.isArray(schemaErrors)) {
      assert.fail('Returned object must be an array')
    }
    assert.strictEqual(schemaErrors.length, 0)
  }

  function shouldFailForWrongObjectFromYamlFile() {
    schemaErrors = validateSchema(wrongPerson, { schemaPath: `${__dirname}/../examples/schema.yaml`, logLevel: 'none' })
    if(!Array.isArray(schemaErrors)) {
      assert.fail('Returned object must be an array')
    }
    assert.strictEqual(schemaErrors.length, 2)
  }

  function shouldFailForWrongObject() {
    let person = { name: { first: 10, last: 'Xoman' }, age: 'Tom' }
    schemaErrors = validateSchema(person, { schema: requiredSchema, logLevel: 'none' })
    if(!Array.isArray(schemaErrors)) {
      assert.fail('Returned object must be an array')
    }
    assert.strictEqual(schemaErrors.length, 3)
  }

  function yamlAndJsonShouldBeEqual() {
    const jsonObject = loadContent(`${__dirname}/../examples/schema.json`)
    const yamlObject = loadContent(`${__dirname}/../examples/schema.json`)
    assert.equal(JSON.stringify(jsonObject), JSON.stringify(yamlObject))
  }


  // running all tests
  shouldFailForWrongObjectFromYamlFile()
  shouldPassForCorrectObjectFromYamlFile()
  shouldPassForCorrectObjectFromFile()
  shouldPassForCorrectObject()
  shouldFailForWrongObject()
  yamlAndJsonShouldBeEqual()
}

testValidateSchema()
